var searchData=
[
  ['old_5frefs',['old_refs',['../classMapping_1_1mapping-4_1_1MAPPING.html#a2f8a9f7fb628c6da0e77f38ad3723183',1,'Mapping::mapping-4::MAPPING']]],
  ['optional',['optional',['../classMapping_1_1mapping-4_1_1MAPPING.html#a10478bc804f6eaf63d6c2efac325fd1f',1,'Mapping::mapping-4::MAPPING']]],
  ['options',['options',['../classMapping_1_1mapper_1_1Bowtie2.html#aa058572bb4e3f6a7b8fa96ce3015e38c',1,'Mapping.mapper.Bowtie2.options()'],['../classMapping_1_1mapper_1_1BWAMem.html#aefad5e610b82826c9ee313c4e482f720',1,'Mapping.mapper.BWAMem.options()']]],
  ['out',['out',['../mapping-4.html#a80aa53875558d9e0fec7bfa4053392e0',1,'Mapping::mapping-4']]],
  ['output',['output',['../classMapping_1_1mapper_1_1Mapper.html#a892899c8ebe81d7b49568c202d4e63d8',1,'Mapping.mapper.Mapper.output()'],['../classMapping_1_1mapper_1_1Bowtie2.html#a5d4056cf2bf5518c16becfe3ce6f6161',1,'Mapping.mapper.Bowtie2.output()'],['../classMapping_1_1mapper_1_1BWAMem.html#ad402a683ca96a31a6348b97398cbd69a',1,'Mapping.mapper.BWAMem.output()'],['../classMapping_1_1metrics_1_1METRICS.html#a98ab6f13420b15454861a7fed91b0f21',1,'Mapping.metrics.METRICS.output()']]]
];

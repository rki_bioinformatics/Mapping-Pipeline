var searchData=
[
  ['bases_5fper_5fcoverage',['bases_per_coverage',['../classMapping_1_1metrics_1_1METRICS.html#a0161e73998dae4a14d8c3f0cea137d27',1,'Mapping::metrics::METRICS']]],
  ['bases_5fper_5fcoverage_5fresult',['bases_per_coverage_result',['../classMapping_1_1metrics_1_1METRICS.html#a4f8651f2c4c88e1a93240d92ed4a78ad',1,'Mapping::metrics::METRICS']]],
  ['best_5frefs',['best_refs',['../classMapping_1_1mapping-4_1_1MAPPING.html#a9ce716c30a212c7768222c921db00d8c',1,'Mapping::mapping-4::MAPPING']]],
  ['bowtie2',['Bowtie2',['../classMapping_1_1mapper_1_1Bowtie2.html',1,'Mapping.mapper.Bowtie2'],['../classMapping_1_1mapper_1_1Bowtie2.html#a08d7d45fea826213bc571cce11815163',1,'Mapping.mapper.Bowtie2.bowtie2()']]],
  ['bwa',['bwa',['../classMapping_1_1mapper_1_1BWAMem.html#aba77b35cfc0fbef0fad19687a44b5664',1,'Mapping::mapper::BWAMem']]],
  ['bwamem',['BWAMem',['../classMapping_1_1mapper_1_1BWAMem.html',1,'Mapping::mapper']]]
];

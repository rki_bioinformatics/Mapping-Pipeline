var searchData=
[
  ['cleanup',['cleanup',['../classMapping_1_1mapping-4_1_1MAPPING.html#a11858665b2fb217e969b6c69aac8f1fe',1,'Mapping.mapping-4.MAPPING.cleanup()'],['../classMapping_1_1mapper_1_1Mapper.html#aa3992579e08ebd9385872824fc5bf118',1,'Mapping.mapper.Mapper.cleanup()'],['../classMapping_1_1mapper_1_1Bowtie2.html#ad842f5ca9da8c45dde4ffeb7af2c203d',1,'Mapping.mapper.Bowtie2.cleanup()'],['../classMapping_1_1mapper_1_1BWAMem.html#ae21896d20764ae25386ca9aeec5c7dc3',1,'Mapping.mapper.BWAMem.cleanup()'],['../classMapping_1_1metrics_1_1METRICS.html#a6db9344a6c279ad264925eeb847c6bb0',1,'Mapping.metrics.METRICS.cleanup()']]],
  ['compute_5fmetrics',['compute_metrics',['../classMapping_1_1metrics_1_1METRICS.html#aa5606da3f093dcd356f13eee5af86169',1,'Mapping::metrics::METRICS']]],
  ['convert_5fto_5faccession',['convert_to_accession',['../classMapping_1_1mapping-4_1_1MAPPING.html#a2b9f6a85b6cd39c7cba7154759a0a8a0',1,'Mapping::mapping-4::MAPPING']]],
  ['convert_5fto_5fbam',['convert_to_bam',['../classMapping_1_1mapping-4_1_1MAPPING.html#ae19fe10528815facfb66d7c520ef4902',1,'Mapping::mapping-4::MAPPING']]],
  ['count_5fcoverage',['count_coverage',['../classMapping_1_1metrics_1_1METRICS.html#ab7dcbe19c263339aed16f7e77b33e98f',1,'Mapping::metrics::METRICS']]],
  ['count_5fmapq',['count_mapq',['../classMapping_1_1metrics_1_1METRICS.html#a4e66b4779386487bce179c2c71f27931',1,'Mapping::metrics::METRICS']]],
  ['coverage_5fgraph',['coverage_graph',['../classMapping_1_1metrics_1_1METRICS.html#a78b4da45ed408011c364b3c6153c9b76',1,'Mapping::metrics::METRICS']]],
  ['coverage_5fstats',['coverage_stats',['../classMapping_1_1metrics_1_1METRICS.html#a3f7b69ff8ef51c3241240587e3d1a050',1,'Mapping::metrics::METRICS']]],
  ['create_5findex',['create_index',['../classMapping_1_1mapper_1_1Bowtie2.html#ae6ef76fbcec9dbaeec73f6c53261addf',1,'Mapping.mapper.Bowtie2.create_index()'],['../classMapping_1_1mapper_1_1BWAMem.html#a3e673f2bd29dfb05afb0aa1c09cd5a2b',1,'Mapping.mapper.BWAMem.create_index()']]]
];

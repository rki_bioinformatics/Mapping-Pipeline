from setuptools import find_packages, setup
import mapping_4.__version__ as vers
import pathlib

HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()


setup(
    name='rki-mapping-pipe',
    packages=find_packages(include=["mapping_4"]),
    version=vers.VERSION,
    description='Pipeline for reference indentification and mapping stats',
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/RKIBioinformaticsPipelines/Mapping",
    tests_require=["pytest","PyYAML", "pandas"],
    install_requires=[
        "snakemake>5.26",
        "strictyaml",
        "PyYAML",
        "biopython",
        "pysam",
        "pandas",
        "numpy",
        "jinja2"],
    entry_points={
        "console_scripts":[
            'mapping-4 = mapping_4.mapping_4:main',
        ]},
    include_package_data = True,
    package_data={
        "covpipe":[
            "covpipe/data/*"]
        },
    author='Enrico Seiler, Wojtek Dabrowski',
    author_email="bi-support@rki.de",
    license='GPLv3',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ]
)


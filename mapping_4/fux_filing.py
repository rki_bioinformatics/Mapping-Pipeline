#!/usr/bin/env python3
#copyright Stephan Fuchs (RKI, FG13, FuchsS@rki.de)
import os
import hashlib
import uuid 
import re
import subprocess
import random
import sys
import time

#############################################################################################

class file():
	'''generic file object:
	CHILDS		
	fastafile			standard fasta file object
	linfastafile		linearized fasta file object
	fastqfile			standard fastq file object
	pairedfastqfile		combines two standard fastq file object containing as paired reads
	logfile				log file object (as I use it)	
	
	PARAMETER
	name:	filename including relative or absolute path [False]. If false, unique 
			filename not existing is generated
	tmp:	tmp status[False]. If true, file is handled as temporary file.
			That means, file is deleted on exit
	mode:	mode of file opening. If mode is false and file exists, mode r 
			(reading from beginning) is set by default. If mode is false and file does 
			not exists or name is false mode is set to w (create and write) by default.
	descr:	file description [False]
	
	ATTRIBUTES:
	descr:			file description [STRING]
	self.name:		file name without path [STRING]
	self.path:		absolute path to file [STRING]
	self.fullname:	absolute path and filename [STRING]
	self.handle:	file handle [FILE RESSOURCE]
	self.mode:		mode of file opening. If false, file is not open [STRING|False]
	self._tmp:		temporary file [BOOL]

	NON-MAGIC METHODS:
	setFilelocation()	extracts name and path from given filename and sets self.name,
						self.path, and self.fullname attributes	
	openFile()			opens file handle with the given mode and sets self.mode attribute
	reopenFile()		reopens file handle with the given mode. Modes w or w+ are replaced 
						by a and a+, respectively, to avoid file truncation. Updates 
						self.mode attribute.
						Dependency: self.openFile()
	closeFile()			closes file handle and sets self.mode to false
	nextLine()			returns next line from file
	changeMode()		changes file opening mode. By default cursor position is not stored 
						but set to the file start and truncation of the file avoided. Updates 
						self.mode attribute.
						Dependency: self.openFile(), self.closeFile()
	renameFile()		renames file on disk and keeps opening mode and state. Only modes 
						w or w+ are replaced by a and a+, respectively, to avoid file 
						truncation.  By default cursor position is not stored but set to the 
						file start. File truncation If path is set file is moved to the new path.
						Updates self.name, self.path, and self.fullname attributes.
						Dependency: self.reopenFile(), self.setFilelocation()
	setTmp()			file is handled as temporary file. That means, file is deleted on exit.
						Filename is extended by .tmp extension. Opening Mode and cursor 
						position are restored. Modes w or w+ are replaced by a and a+, respectively, 
						to avoid file truncation. Attributes self.name, self.path, self.fullname,
						self.mode, self._tmp are updated.
						Dependency: renameFile(), setFilelocation()
	unsetTmp()			file handled as non-temporary file. If file extension is .tmp extension is
						removed. Opening Mode and cursor position are restored. Modes w or w+ 
						are replaced by a and a+, respectively, to avoid file truncation. 
						Attributes self.name, self.path, self.fullname,	self.mode, self._tmp 
						are updated.
						Dependency: renameFile(), setFilelocation()			
	checkFile()			[static] checks if file exists. Only parameter is file name including path. 
						returns True or False.
	uniqueFilename()	[static] returns a non-existent 8 chars long filename based on uuid4. 
						If path parameter is not submitted, the actual path is selected.
	fuseFiles()			[class] fuses files returns full name of the fused file (filename + path). 
						Files are submitted as list. Name and Path of the fused file can 
						be also submitted.
	destroy()			destroys file object by closing file handle and in case of temporary
						file deleting file from disk.
						Dependency: self.closeFile()	
	write()				write to file
	flush()				write buffer to file
	'''
	
	def __init__(self, name = False, tmp = False, mode = False, descr = False):
		self._tmp = tmp
		self.descr = descr

		if name == False:
			name = self.uniqueFilename()
			if self._tmp:
				name += '.tmp'
				
		self.setFilelocation(name)
		
		if mode:
			self._mode = mode		
		else:
			if self.checkFile(self.fullname):
				self._mode='r'
			else:
				self._mode='w'				
		if self._mode:
			self.openFile(self._mode)				
		
	def __enter__(self):
		return self		
			
	def setFilelocation(self, fullname = False):
		'''sets or updates name, path, full name attributes'''		
		if fullname:
			self.path, self.name = os.path.split(fullname)
		self.path = os.path.abspath(self.path)
		self.fullname = os.path.abspath(os.path.join(self.path, self.name))
		
	def openFile(self, mode):
		'''opens file handle'''
		self._mode = mode
		self.handle = open(self.fullname, self._mode)	
		
	def reopenFile(self, mode):
		'''reopens file handle. Modes w or w+ are replaced by a and a+, respectively, to avoid file truncation'''
		self._mode = mode
		if mode == 'w':
			mode = 'a'
		elif mode == 'w+':
			mode = 'a+'
		self.handle = open(self.fullname, self._mode)		
		
	def closeFile(self):
		'''closes file handle'''
		if self.handle.closed == False:
			self._mode = False
			self.handle.close()

	def nextLine(self):		
		'''returns next line from file. Does not check if file is open for reading!'''
		return self.handle.readline()
		
	def changeMode(self, mode, keepCursor = False, truncate = False):
		'''
		changes the mode the file is opened.
		Parameter:	
		mode		new mode
		keepCursor	Pointer within the file is kept. If False (default) cursor is set to beginning of the file.
		truncate	if False (default) truncating modes (w, w+) are replaced by appending modes (a, a+)
		'''
		cursor = 0
		if truncate == False:
			if mode == 'w':
				mode = 'a'
			elif mode == 'w+':
				mode = 'a+'		
		if self.handle.closed == False:
			if keepCursor:
				cursor = self.handle.tell()
			self.closeFile()
		if mode:
			self.openFile(mode)
			if cursor > 0:
				self.handle.seek(cursor)			
			
	def renameFile(self, newfilename, path = False, keepCursor = False):
		''' Renames a file. File is opened again if it was open (mode is restored). If keepCursor is True cursor is set to the last curor position within the file.'''
		if path == False:
			path = self.path
		cursor = 0
		mode = self._mode
		self.handle.close()
		if self.handle.closed == False:
			if keepCursor:
				cursor = self.handle.tell()	
			self.closeFile()		
		newfullname = os.path.join(path, newfilename)		
		if self.checkFile(self.fullname):
			os.rename(self.fullname, newfullname)
		self.setFilelocation(newfullname)		
		if mode:
			self.reopenFile(mode)
		if cursor > 0:
			self.handle.seek(cursor)				
	
	def setTmp(self):
		'''Set fileObject tmp attribute True'''	
		if self._tmp == False:
			if self.name[-4:] != '.tmp':
				newfilename = self.name + '.tmp'
				self.renameFile(newfilename, keepCursor = True)
				self.name = newfilename
				self.setFilelocation()
			self._tmp = True
	
	def unsetTmp(self):
		'''Set fileObject tmp attribute False'''
		if self._tmp == True:
			if self.name[-4:] == '.tmp':
				newfilename = self.name[:-4]
				self.renameFile(newfilename, keepCursor = True)
				self.name = newfilename
				self.setFilelocation()				
		self._tmp = False	
	
	def write(self, str):
		'''write to file'''
		self.handle.write(str)
		
	def flush(self):
		'''write buffer to file'''
		self.handle.flush()		
		os.fsync(self.handle.fileno())
	
	@staticmethod  
	def checkFile(fullname):
		'''checks if file exists. Only parameter is file name including path. Returns True or False.'''		
		return os.path.isfile(fullname)
	
	@staticmethod  		
	def uniqueFilename(path=os.getcwd(), ext = ''):
		'''provides a filename based on uuid not in use. If no path parameter is used, the current working dir is used. Returns proposed filename including path as string.'''
		if ext != '':
			ext = '.' + ext
		while True:
			fullname = os.path.join(path, str(uuid.uuid4())[:8]) + ext
			if file.checkFile(fullname) == False:
				break
		return fullname		
		
	@classmethod    
	def fuseFiles(cls, childs, name=False, path=os.getcwd()):
		'''
		Creates a fused file. Returns a full filename (including path) of fused file.
		Parameter:
		childs	list of file names (including paths) whose content should be fused
		name	filename of fused file (including path). File will be overwritten! If false, a unique filename is created
		path	path for fused file (default: current working dir).		
		'''
		pass
		if name == False:
			fullname = cls.uniqueFilename(path)
		else:
			fullname = os.path.join(path, name)
		
		with open(fullname, "wb") as outfile:
			for file in childs:
				with open(file, "rb") as infile:
					while True:
						content = infile.read(100000000)
						if not content:
							break
						outfile.write(content)
				outfile.write(b'\n')
		return fullname
		
	
	def destroy(self):
		if self.handle.closed == False:
			self.closeFile()	
		if self._tmp:
			try:		
				os.remove(self.fullname) 
			except:
				pass
	
	def __exit__(self, type, value, traceback):
		self.destroy()

#############################################################################################
			
class fastafile(file):	
	'''standard fasta file object
	PARENTS
	file	generic file object
	
	PARAMETER:
	name:	filename including relative or absolute path [False]. If false, unique 
			filename not existing is generated
	tmp:	tmp status[False]. If true, file is handled as temporary file.
			That means, file is deleted on exit
	mode:	mode of file opening. If mode is false and file exists, mode r 
			(reading from beginning) is set by default. If mode is false and file does 
			not exists or name is false mode is set to w (create and write) by default.
	descr:	file description [False]
		
	ATTRIBUTES:
	self._number_header 		Number of fasta head lines in the file*
	self._number_singleheader	Number of fasta single headers in the file*
								Fasta head lines can contain multiple headers separated
								by SOH (chr(1))*
	self._number_comments		Number of comment lines in the file*
	self.number_accessions		Number accessions in the file*
	self._accessions			set of accessions in the file*
	
	* to avoid unnecessary (and sometimes time consuming) parsing, attributes are generated if
	needed the first time	

	NON-MAGIC METHODS:
	getHeaderNumber()				returns number of fasta headers in the file
	getSingleHeaderNumber()			returns number of single headers in the file
	getCommentNumber()				returns number of comment lines in the file
	screenFASTA()					calculates number of headers, single headers, and comments in the file.
									For this opening mode is changed to r and cursor set to the beginning of
									the file. Importantly, original opening	mode and cursor positions are
									restored after actions. However, original modes w or w+ are replaced 
									by a and a+, respectively, to avoid file truncation. Updates 
									self._number_header, self._number_singleheader, and self._number_comments. 
									No return value.
									Dependency:  self.changeMode(), self.isComment(), self.isHeader()	self.isHeader
	nextEntry()						Returns next entry of FASTA file as tuple (header, seq, comment). Cursor 
									is set immediatly before the following entry
									Dependency: self.isComment(), self.isHeader
	isHeader()						returns true if submitted string is a valid fasta header, otherwise false.
	isComment()						returns true if submitted string is a fasta comment, otherwise false.
	makeNR()						fuses fasta entries with the same full-length sequence to one entry with
									a multiple header. Returns a fasta file object pointing to the generated NR 
									(non-redundant) file.
									Dependency: fastafile(), linfastafile(), self.linearize(), self.changeMode()
	linearize()						generates a linearized representation of a fasta file. Each line represents an
									entry organized in columns: header, seq, comment, seqlen (optionally)
									Separator of columns is \t by default and masked in headers and comments using
									space by default. Returns a non-temporary linfastafile object.
									Dependency: self.changeMode()
	getSeqHash()					[static] returns an UTF-8 encoded MD5 hash of a given sequence (string)
	getAccessions()					screens file for accessions of type: accession_prefix|accession_number
									Accession prefixes have submitted as list. For this opening mode is changed 
									to r and cursor set to the beginning of	the file. Importantly, original opening	mode 
									and cursor positions are restored after actions. However, original modes w or w+ 
									are replaced by a and a+, respectively, to avoid file truncation. Updates 
									self.number_accessions, self._accessions
									No return value.
	'''
	
	def __init__(self, name = False, tmp = False, mode = False, descr = False):		
		file.__init__(self, name = name, tmp = tmp, mode = mode, descr = descr)
		self._number_header = False
		self._number_singleheader = False
		self._number_comments = False	
		self.number_accessions = False
		self._accessions = False	

	def getHeaderNumber(self):
		if self._number_header == False:
			self.screenFASTA()
		return self._number_header
		
	def getSingleHeaderNumber(self):
		if self._number_singleheader == False:
			self.screenFASTA()
		return self._number_singleheader		
		
	def getCommentNumber(self):
		if self._number_comments == False:
			self.screenFASTA()
		return self._number_comments	
	
	def screenFASTA(self):
		'''provides number of entries and single header as attributes'''
		prevmode = self._mode
		cursor = self.handle.tell()
		self.changeMode('r')
		self._number_header = 0
		self._number_singleheader = 0
		self._number_comments = 0
		for line in self.handle:
			if self.isHeader(line):
				self._number_header += 1
				self._number_singleheader += 1 + line.count(chr(1))
			elif self.isComment(line):
				self._number_comments += 1	
		self.changeMode(prevmode)				
		self.handle.seek(cursor)	
		
	def nextEntry(self):
		'''Returns next entry of FASTA file as tuple (header, seq, comment). File pointer is set immediatly before the following entry'''
		e = 0
		seq = []
		comment=[]
		header = False
		while e < 2:
			cursor = self.handle.tell()			
			#read line
			line = self.nextLine()
			if not line:
				break
			line = line.strip()			
			#header detection
			if self.isHeader(line):
				e += 1
				# entry start
				if e == 1:
					header = line
				#entry end
				else:
					self.handle.seek(cursor)
					break
			elif self.isComment(line):
				comment.append(line)
			else:
				seq.append(line)		
		if header == False:
			return False
		else:
			return (header, ''.join(seq), ' '.join(comment))
			
	def isHeader(self, str):
		#str = str.lstrip()
		if len(str) > 0 and str[0] == '>':
			return True
		else:
			return False
			
	def isComment(self, str):
		#str = str.lstrip()
		if len(str) > 0 and str[0] == ';':
			return True
		else:
			return False		
			
	def makeNR(self, name=False, path=os.getcwd(), format = 'fasta'):
		#linearize and add seqlen + seqhash
		with self.linearize(addSeqLen=True) as linFile:
			linFile.setTmp()
			#sort by length and seq
			with linfastafile(tmp = True, mode = False) as sortedFile:					
				cmd = ['sort',  '-k', '4,2', '-t', '\t', '-o' , sortedFile.fullname, linFile.fullname] #linux
				cmd = ['S:\\cygwin64\\bin\\sort.exe', '-k', '4,2', '-t', '	', '-o' , sortedFile.fullname, linFile.fullname] #cygwin
				#print (" ".join(cmd))
				subprocess.check_call(cmd)
				
				#compare seqs in sorted file and make NR fasta
				if format == 'fasta':
					nrfile = fastafile()
				else:
					nrfile = linfastafile()		
				
				sortedFile.changeMode('r')
				length = -1
				seq = ''
				header = []
				for line in sortedFile.handle:
					line = line.strip()
					if len(line) > 0:
						fields = line.split('\t')
						if int(fields[3]) == length and fields[1] == seq:
								header.extend(fields[0][1:].split(chr(1)))
						else:
							if len(header) > 0:
								nrfile.handle.write(">" + chr(1).join(set(header)) + '\n' + seq + '\n')
							header = fields[0][1:].split(chr(1))
							seq = fields[1].strip()
							length = int(fields[3])
				nrfile.handle.write(">" + chr(1).join(set(header)) + '\n' + seq + '\n')
		return nrfile	
		

	def linearize(self, name = False, sep = '\t', sepmask = ' ', addSeqLen = False):
		'''
		Saves a linearized copy of a FASTA file and returns a associated linfastafile Object.
		Options:
		sep			separator used between columns of the linearized file (by default: \t)
		sepmask		replacement of naturally occuring separators (by default: space)
		addSeqLen	Sequence length is added to file
		Output:
		linfastafile Object point to a text file with different columns
		col1	header
		col2	sequence
		col3	comment
		col4	sequence length (if addSeqLen = True)
		'''
		prevmode = self._mode
		cursor = self.handle.tell()
		self.changeMode('r')
		out = []
		i = 0
		
		with linfastafile(name, sep, sepmask, tmp=False) as linfile:
			while True:
				i += 1
				entry = self.nextEntry()
				if entry == False:
					break			
				entry=[x.replace(sep, sepmask) for x in list(entry)]
				if addSeqLen:
					entry.append(str(len(entry[1])))
				out.append(sep.join(entry))		
				if i >= 1000000:
					linfile.handle.write('\n'.join(out) + '\n')
					out = []
					i = 0
			linfile.handle.write('\n'.join(out) + '\n')
			self.changeMode(prevmode)
			self.handle.seek(cursor)
			linfile.closeFile()
		return linfile
		
	@staticmethod
	def getSeqHash(seq):
		'''returns MD5 hash of sequence '''
		return hashlib.md5(seq.encode('utf-8')).hexdigest()		

	
	def getAccessions(self, actypes):
		if self._accessions:
			return self._accessions
		self._accessions = set()	
		prevmode = self._mode
		cursor = self.handle.tell()
		self.changeMode('r')
		self.number_accessions = 0
		regexp = r"(?:>|" + chr(1) + "|\|)(?:" + "|".join(actypes) + ")\|[^|\r\n ]+"
		pattern = re.compile(regexp)
		for line in self.handle:
			line = line.strip()
			if self.isHeader(line):
				matches = re.findall(pattern, line)
				if matches:
					self._accessions.update(matches)
		self.number_accessions = len(self._accessions)	
		self.changeMode(prevmode)				
		self.handle.seek(cursor)		
		
#############################################################################################
		
class linfastafile(file):
	'''linearized fasta file object
	these files are separator-delimited representations of FASTA files usefull for parsing
	each line represents one entry
		
	PARENTS
	file	generic file object
	
	PARAMETER:
	name:		filename including relative or absolute path [False]. If false, unique 
				filename not existing is generated
	tmp:		tmp status[False]. If true, file is handled as temporary file.
				That means, file is deleted on exit
	mode:		mode of file opening. If mode is false and file exists, mode r 
				(reading from beginning) is set by default. If mode is false and file does 
				not exists or name is false mode is set to w (create and write) by default.
	descr:		file description [False]
	sep:		separator of columns [CHAR]
	sepmask:	char used to mask separator in headers or comments [CHAR]. Must be different from sep
	
		
	ATTRIBUTES:
	self._number_header 		Number of fasta head lines in the file*
	self._number_singleheader	Number of fasta single headers in the file*
								Fasta head lines can contain multiple headers separated
								by SOH (chr(1))*
	self._number_comments		Number of comment lines in the file*
	self._number_accessions		Number accessions in the file*
	self._accessions			set of accessions in the file*
	self.number_seq 			Number of sequences*
	
	* to avoid unnecessary (and sometimes time consuming) parsing, attributes are generated if
	needed the first time	

	NON-MAGIC METHODS:
	initialize()		cursor is set to position 0
						atrributes self._number_header, self._number_seq, self._number_singleheader,
						self._number_comments are set to False
	getHeaderNumber()				returns number of fasta headers in the file
									Dependency: self.screenLinFASTA()
	getSingleHeaderNumber()			returns number of single headers in the file
									Dependency: self.screenLinFASTA()
	getCommentNumber()				returns number of comments in the file
									Dependency: self.screenLinFASTA()
	getSequenceNumber()				returns number of sequences in the file
									Dependency: self.screenLinFASTA()
	screenLinFASTA()				calculates number of headers, single headers, and comments in the file.
									For this opening mode is changed to r and cursor set to the beginning of
									the file. Importantly, original opening	mode and cursor positions are
									restored after actions. However, original modes w or w+ are replaced 
									by a and a+, respectively, to avoid file truncation. Updates 
									self._number_header, self._number_singleheader, and self._number_comments. 
									No return value.
									Dependency:  self.changeMode()
	getAccessions()					screens file for accessions of type: accession_prefix|accession_number
									Accession prefixes have submitted as list. For this opening mode is changed 
									to r and cursor set to the beginning of	the file. Importantly, original opening	mode 
									and cursor positions are restored after actions. However, original modes w or w+ 
									are replaced by a and a+, respectively, to avoid file truncation. Updates 
									self.number_accessions, self._accessions
									No return value.	
	'''
	
	def __init__(self, name = False,  sep = '\t', sepmask = ' ', tmp=False, mode = False, descr = False):	
		file.__init__(self, name=name, tmp=tmp, mode=mode, descr = descr)
		self.number_header = 0
		self._sep = sep
		self._sepmask = sepmask
		self.number_header = False
		self.number_seq = False
		self.number_singleheader = False
		self.number_comments = False
		self._accessions = False
		self.number_accessions = False
		
		self.initialize()

	def initialize(self):
		'''provides number of entries and single header as attributes'''
		filepointer = self.handle.tell()
		self.handle.seek(0)
		self._number_header = False
		self._number_seq = False
		self._number_singleheader = False
		self._number_comments = False
		
	def getHeaderNumber(self):
		if self._number_header == False:
			self.screenLinFASTA()
		return self.number_header
		
	def getSingleHeaderNumber(self):
		if self._number_singleheader == False:
			self.screenLinFASTA()
		return self._number_singleheader		
		
	def getCommentNumber(self):
		if self._number_comments == False:
			self.screenLinFASTA()
		return self._number_comments	

	def getSeqNumber(self):
		if self.number_seq == False:
			self.screenLinFASTA()
		return self.number_seq		
	
	def screenLinFASTA(self):
		'''provides number of entries and single header as attributes'''
		prevmode = self._mode
		cursor = self.handle.tell()
		self.changeMode('r')
		self.number_header = 0
		self.number_seq = 0
		self.number_singleheader = 0
		self.number_comments = 0
		for line in self.handle:
			if len(line) > 0:
				fields = [x.strip() for x in line.split(self._sep)]
				if len(fields[0]) > 0:
					self.number_header += 1
					self.number_singleheader += 1 + fields[0].count(chr(1))
				if len(fields[1]) > 0:
					self.number_seq += 1
				if len(fields[2]) > 0:
					self.number_comment += 1
		self.changeMode(prevmode)				
		self.handle.seek(cursor)			


	def getAccessions(self, actypes):
		if self._accessions:
			return self._accessions
		self._accessions = set()			
		filepointer = self.handle.tell()		
		self.handle.seek(0)
		self.number_accessions = 0
		regexp = r"(?:>|" + chr(1) + "|\|)(?:" + "|".join(actypes) + ")\|[^|\r\n ]+"
		pattern = re.compile(regexp)
		for line in self.handle:
			matches = re.findall(pattern, line.split(self._sep)[0])
			if matches:
				self._accessions.update(matches)
		self.handle.seek(filepointer)		
		self.number_accessions = len(self._accessions)
		
#############################################################################################
		
class fastqfile(file):
	'''standard fastq file object
	
	PARENTS
	file	generic file object
	
	PARAMETER:
	name:		filename including relative or absolute path [False]. If false, unique 
				filename not existing is generated
	tmp:		tmp status[False]. If true, file is handled as temporary file.
				That means, file is deleted on exit
	mode:		mode of file opening. If mode is false and file exists, mode r 
				(reading from beginning) is set by default. If mode is false and file does 
				not exists or name is false mode is set to w (create and write) by default.
	descr:		file description [False]
	
		
	ATTRIBUTES:
	self._number_reads	number of reads in the file
	
	* to avoid unnecessary (and sometimes time consuming) parsing, attributes are generated if
	needed the first time	

	NON-MAGIC METHODS:
	initialize()					cursor is set to position 0
									atrribute self._number_reads is set to False
	getReadNumber()					returns number of reads in the file
									Dependency: self.screenFastq()
	checkIdentifierLine()			returns true if given string starts with @
	checkDescriptionLine()			returns true if given string starts with +
	screenFastq()					calculates number of headers, single headers, and comments in the file.
									For this opening mode is changed to r and cursor set to the beginning of
									the file. Importantly, original opening	mode and cursor positions are
									restored after actions. However, original modes w or w+ are replaced 
									by a and a+, respectively, to avoid file truncation. Updates 
									self._number_header, self._number_singleheader, and self._number_comments. 
									No return value. Can exit and print Exception message if standard format is 
									violated.
									Dependency:  self.changeMode()
	nextRead()						Returns next read of FASTQ file as tuple (identifier, sequence, description, quality scores). 
									File pointer is set immediatly before the following entry
									Dependency:  self.nextLine()
	randomSubset()					generates a fastq file with a random read subset. Proportion [0, 1] can be defined.
									Returns a new fastq file object.
									Dependency: self.nextRead(), self.getReadNumber()
	'''
	
	def __init__(self, name = False, tmp=False, mode = False, descr = False):	
		file.__init__(self, name=name, tmp=tmp, mode=mode, descr = descr)	
		self.initialize()

	def initialize(self):
		'''provides number of entries and single header as attributes'''
		filepointer = self.handle.tell()
		self.handle.seek(0)
		self._number_reads = False
		
	def getReadNumber(self):
		'''returns number of reads stored in the fastq file'''
		if self._number_reads == False:
			self.screenFastq()
		return self._number_reads
		
	def checkIdentifierLine(self, line):
		if line[0] == "@":
			return True
		else:
			return False
			
	def checkDescriptionLine(self, line):
		if line[0] == "+":
			return True
		else:
			return False				
	
	def screenFastq(self):
		'''provides number of reads as attributes'''
		prevmode = self._mode
		cursor = self.handle.tell()
		self.changeMode('r')
		self._number_reads = 0
		l = 0
		while True:
			identifier = self.handle.readline()
			l += 1
			if not identifier:
				break
			if identifier == "":
				continue
			if self.checkIdentifierLine(identifier) == False:
				print("FORMAT EXCEPTION in line", l, ": identifier lines should start with @")
				self._number_reads = False
				exit(1)
			self.nextLine()
			l += 1
			if self.checkDescriptionLine(self.nextLine()) == False:
				print("FORMAT EXCEPTION in line", l , ": description lines should start with +")
				self._number_reads = False
				exit(1)			
			l += 1
			if self.nextLine() == "":
				print("FORMAT EXCEPTION in line", l , ": empty score line")
				self._number_reads = False
				exit(1)			
			l += 1			
			self._number_reads += 1
		self.changeMode(prevmode)				
		self.handle.seek(cursor)

	def nextRead(self):		
		'''Returns next read of FASTQ file as tuple (identifier, sequence, description, quality scores). File pointer is set immediatly before the following entry'''
		line = self.nextLine()
		if not line:
			return False
		if line.strip() == "":
			self.nextRead()
		if self.checkIdentifierLine(line) == False:
			print("FORMAT EXCEPTION: identifier lines should start with @")
			exit(1)
		identifier = line
		
		line = self.nextLine()
		if line.strip() == '':
			print("FORMAT EXCEPTION: no read sequence found")
			exit(1)			
		seq = line
		
		line = self.nextLine()
		if self.checkDescriptionLine(line) == False:
			print("FORMAT EXCEPTION: description lines should start with +")
			exit(1)			
		descr = line
		
		line = self.nextLine()
		if line.strip() == '':
			print("FORMAT EXCEPTION: no quality scores found")
			exit(1)			
		scores = line
		
		return (identifier, seq, descr, scores)
		
	def randomSubset(self, prop, tmp = False, selected = False):
		'''generates a fastq file with a random read subset. Proportion [0, 1] can be defined. Returns a new fastq file object.'''
		if selected == False:
			prop = int(self.getReadNumber()*prop)
			selected = random.sample(range(1, self.getReadNumber()), prop)
		selected = set(selected)
		
		subsetfastqObj = fastqfile(tmp = tmp)
		e = 1
		while e <= self.getReadNumber():
			readdata = self.nextRead()
			if e in selected:
				subsetfastqObj.handle.write(''.join(readdata))
			e += 1
		return subsetfastqObj
		
#############################################################################################	

class pairedfastqfiles():
	'''combines two standard fastq file object containing as paired reads
	read numbers have to be the same in both files
	
	PARENTS
	file	generic file object
	
	PARAMETER:
	fname:		filename of forward read file including relative or absolute path [False]. If false, unique 
				filename not existing is generated
	rname:		filename of reverse read file including relative or absolute path [False]. If false, unique 
				filename not existing is generated				
	tmp:		tmp status[False]. If true, files are handled as temporary files.
				That means, files are deleted on exit
	mode:		mode of file opening. If mode is false and file exists, mode r 
				(reading from beginning) is set by default. If mode is false and file does 
				not exists or fname/rname is false mode is set to w (create and write) by default.
	descr:		file description [False]
	
		
	ATTRIBUTES:
	self.forward		fastq file object of the forward read file
	self.reverse		fastq file object of the reverse read file
	self._number_reads	read numbers (same for both files)
	
	NON-MAGIC METHODS:
	initialize()					conmpares read numbers of paired files. Exception shown if different.
									Cursor is NOT set to position 0
									Updates self._number_reads
									Dependency: self.getReadNumber()
									attribute self._number_reads is set to False
	getReadNumber()					returns number of reads  in the files
									Dependency: self.screenLinFASTA()
	randomSubset()					generates paired fastq files with random paired read subsets. Proportion [0, 1] can be defined.
									Returns a new paired fastq file object.
									Dependency: self.nextRead(), self.getReadNumber()
	'''	
	
	def __init__(self, fname = False, rname = False, tmp=False, mode = False, descr = False):	
		self.forward = fastqfile(name=fname, tmp=tmp, mode=mode, descr = descr)
		self.reverse = fastqfile(name=rname, tmp=tmp, mode=mode, descr = descr)
		self.initialize()
	
	def __enter__(self):
		return self			

	def initialize(self):
		'''provides number of entries and single header as attributes'''
		self.getReadNumber()
		
	def getReadNumber(self):
		if self.forward.getReadNumber() != self.reverse.getReadNumber():
			print('forward file:', self.forward.fullname, '(reads:', self.forward.getReadNumber(),')')
			print('reverse file:', self.reverse.fullname, '(reads:', self.reverse.getReadNumber(),')')
			raise ValueError('paired files differ in read numbers')
		else:
			self._number_reads = self.forward.getReadNumber()*2
		return self._number_reads
			
	def randomSubset(self, prop, tmp = False):
		'''generates paired fastq files with random read subsets. Proportion [0, 1] can be defined. Returns a new paired fastq file object.'''
		prop = int(self.getReadNumber()*prop)
		selected = random.sample(range(1, self.getReadNumber()), prop)
		forward_subset = self.forward.randomSubset(prop, tmp = tmp, selected = selected)
		forward_subset.closeFile()
		reverse_subset = self.reverse.randomSubset(prop, tmp = tmp, selected = selected)
		reverse_subset.closeFile()
		subsetPairedfastqObj = pairedfastqfiles(fname = forward_subset.fullname, rname = reverse_subset.fullname, tmp = tmp)
		return subsetPairedfastqObj		
		
	def setTmp(self):
		'''Set fileObject tmp attribute True'''	
		self.forward.setTmp()		
		self.reverse.setTmp()
	
	def unsetTmp(self):
		'''Set fileObject tmp attribute False'''
		self.forward.unsetTmp()		
		self.reverse.unsetTmp()		
		
	def __exit__(self, type, value, traceback):
		self.forward.destroy()		
		self.reverse.destroy()		
			
			
	
#############################################################################################	
		
class logfile(file):
	'''log file object (as I use it)
	
	PARENTS
	file	generic file object
	
	ATTRIBUTES
	catlineChar		Char or string used as category underlin [STRING]
	catlineLen 		Length category underlining [INT]
			
	PARAMETER:
	name:		filename including relative or absolute path [False]. If false, unique 
				filename not existing is generated
	tmp:		tmp status[False]. If true, file is handled as temporary file.
				That means, file is deleted on exit
	mode:		mode of file opening. If mode is false and file exists, mode r 
				(reading from beginning) is set by default. If mode is false and file does 
				not exists or name is false mode is set to w (create and write) by default.
	descr:		file description [False]
	
		
	ATTRIBUTES:
	self.catlineLen		length of category underlining
	self.catlineChar	char used for category underlining

	NON-MAGIC METHODS:
	initialize()					cursor is set to position 0
									atrribute self._number_reads is set to False
									
	writeAsCols()					create columns with individual width, text is left aligned, numbers 
									right aligned. Formatted string is written to log file. Input is a 2-level 
									list (1st level: lines, 2nd level: columns). Returns no value.
	newCat()						writes a new category with a given Name to the log file incl. underlining 
									(s. attributes)
	lb()							writes a linebreak to logfile. For compatibility reasons windows line 
									breaks (\r\n) are used
									Dependency: self.write()
	addExecTime()					adds the execution time in hh:ss:mm to log file. Seconds have to be given.
									Dependency: self.write(), self.lb()
	
	'''
	
	def __init__(self, name = False, tmp=False, mode = False, descr = False):	
		file.__init__(self, name=name, tmp=tmp, mode=mode, descr = descr)	
		self.catlineLen = 30
		self.catlineChar = "="
		
	def writeAsCols (self, input, sep = ' ' * 5):
		'''create columns with individual width
		
		PARAMETER		
		sep 		separator of columns [STRING]
		'''
		#allowed signs in numbers
		pattern = re.compile(r'([+-])?([0-9]+)([.,]?)([0-9]+)(%?)')
    
		#str conversion
		lines = [[str(x) for x in line] for line in input] 
    
		#fill up rows to same number of columns (important for transposing)
		maxlen = max([len(x) for x in lines])
		[x.extend(['']*(maxlen-len(x))) for x in lines]    
		
		#find format parameters
		width = [] #colum width or length
		align = [] #alignment type (number = r; strings = l)
		prec = [] #number precision
		for column in zip(*lines):
			width.append(len(column[0]))
			prec.append(0)
			align.append('r')
			for field in column[1:]:
				if align[-1] ==  'l':
					width[-1] = max(width[-1], len(field))
				else:
					match = pattern.match(field)
					if match and match.group(0) == field:
						if match.group(3) and match.group(4):
							prec[-1] = max(prec[-1], len(match.group(4)))
						
						if prec[-1] > 0:
							k = 1
						else:
							k = 0
						width[-1] = max(width[-1], len(match.group(2)) + prec[-1] + k + len(match.group(4)))
					else:
						align[-1] = 'l'
						prec[-1] = False
						width[-1] = max(width[-1], len(field))
				
		#formatting
		output = []
		for	line in lines:
			f = 0
			output.append([])
			for field in line:
				if align[f] == 'l':
					output[-1].append(field + ' ' * (width[f] - len(field)))
				elif align[f] == 'r':
					match = pattern.match(field)
					if not match or match.group(0) != field: 
						output[-1].append(' ' * (width[f] - len(field)) + field)
					else:
						if match.group(5):
							percent = match.group(5)
						else:
							percent = ''
						length = len(percent)
						
						intpart = match.group(2)
						if match.group(1):
							intpart = match.group(1) + intpart
						if match.group(3):
							decpart = match.group(4) 
						else:
							intpart += match.group(4)
							decpart = ''							
						length += len(intpart)
						
						#print('match:', match.group(0))
						#print('intpart:', intpart)
						#print('decpart:', decpart)
						#print('prec:', prec[f])
						
						if prec[f] > 0:
							decpart += '0' * (prec[f] - len(match.group(4)))
							length += len(decpart) + 1
							formatted_number = ' ' * (width[f] - length) + intpart + '.' + decpart + percent
						else:
							formatted_number = ' ' * (width[f] - length) + intpart + percent
						output[-1].append(formatted_number)		
				f += 1
		self.write('\r\n'.join([sep.join(x) for x in output]))
		
	def newCat(self, cat):			
		if self.catlineLen < len(cat):
			catline = self.catlineChar * len(cat)
		else:
			catline = self.catlineChar * self.catlineLen 
		self.write('\r\n\r\n' + cat.upper() + '\r\n' + catline + '\r\n')

	def lb(self):			
		self.write('\r\n')	

	def addExecTime(self, s):
		self.lb()
		self.lb()
		self.write('--------------------------')
		self.lb()
		self.write('execution time (hh:mm:ss): ') 
		self.write('{:02.0f}:{:02.0f}:{:02.0f}'.format(s//3600, s%3600//60, s%60))			

		
		
							
						
						
						
					
					
